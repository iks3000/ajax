const VALID_EMAIL_ENDINGS = ["gmail.com", "outlook.com", "yandex.ru"];

export const validate = (email) => {
    let value = email.value;
    const inputEnding = value.substring(
        value.indexOf("@") + 1
    );
    return VALID_EMAIL_ENDINGS.some((value) => value === inputEnding);
}

